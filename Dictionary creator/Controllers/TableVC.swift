//
//  TableVC.swift
//  Dictionary creator
//
//  Created by Юрий Ершов on 09.08.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

import UIKit
import StoreKit

class TableVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var FirstScreenInfo = FirstScreenVC()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FirstScreenInfo.arrayUnicWord.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        switch FirstScreenInfo.sorted {
        case true:
            cell.textLabel?.text = FirstScreenInfo.arrayUnicWord.sorted()[indexPath.row]
        case false:
            cell.textLabel?.text = FirstScreenInfo.arrayUnicWord[indexPath.row]
        }
        cell.backgroundColor = .clear
        cell.textLabel?.textAlignment = .center
        return cell
    }
    
    @IBAction func BackButtonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "Home", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Home" {
            var pageAdd = segue.destination as! FirstScreenVC
            pageAdd = FirstScreenInfo
            print("Exit")
            dismiss(animated: true, completion: nil)
        }
        
    }

}
