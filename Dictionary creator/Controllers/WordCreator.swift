//
//  WordCreator.swift
//  Dictionary creator
//
//  Created by Юрий Ершов on 08.08.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

import Foundation

public var simbols: [String] = [" ", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "-", "_", "\"", "'", "+", "-", "±", "§", ",", ".", "?", "<", ">", ";", ":", "|", "/", "{", "}", "=", "[", "]", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "\n"]

public func clearWordMassive (text: String) -> [String] {
    
    var DictionaryRow: [String] = []
    var newWord: String = ""
    var newSimbol: Character?

    
    DictionaryRow.removeAll()
    newSimbol = nil

        for value in text.lowercased()  {
            
            if simbols.contains(String(value)) == false {
                newSimbol = value
            }
            
            if simbols.contains(String(value)) == true {
                DictionaryRow.append(newWord); newWord.removeAll()
            }
            
            if let Simb = newSimbol { newWord.append(Simb) }
            newSimbol = nil
        }
        

    return DictionaryRow
}

public func textFromArrayText (arrayText: [String], separator: String) -> String {
    
        var DictionaryAsText = ""
        for word in arrayText {
            DictionaryAsText.append("\(word.lowercased())\(separator)")
        }
    return DictionaryAsText
}


public func getUnicWord (arrayText: [String]) -> [String] {
    var UnicDictionary: [String] = []
    
    for word in arrayText {
        if UnicDictionary.contains(word) == false {
            UnicDictionary.append(word)
        }
    }
    return UnicDictionary
}
