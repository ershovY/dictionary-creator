//
//  ViewController.swift
//  Dictionary creator
//
//  Created by Юрий Ершов on 08.08.2019.
//  Copyright © 2019 smartProject. All rights reserved.
//

import UIKit
import StoreKit

class FirstScreenVC: UIViewController {

    
    @IBOutlet weak var TextLabelFirst: UILabel!
    @IBOutlet weak var TextLabelSecond: UILabel!
    @IBOutlet weak var ActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var SortedButtonOutlet: UIButton!
    
    var arrayWords: [String] = []
    var arrayUnicWord: [String] = []
    var text: String = ""
    var separators: [String] = ["space", " - ", " _ ", "/n ", " : ", "*"]
    var separator: String = " "
    var sorted = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        TextLabelFirst.layer.cornerRadius = 10
        TextLabelSecond.layer.cornerRadius = 10
        TextLabelFirst.clipsToBounds = true
        TextLabelSecond.clipsToBounds = true
        
    }

    @IBAction func CreateButton(_ sender: UIButton) {
        
        ActivityIndicator.isHidden = false
        ActivityIndicator.startAnimating()
        TextLabelSecond.text?.removeAll()
        arrayWords = clearWordMassive(text: TextLabelFirst.text!)
        arrayUnicWord = getUnicWord(arrayText: arrayWords)
        switch sorted {
        case true:
            text = textFromArrayText(arrayText: arrayUnicWord.sorted(), separator: separator)

        case false:
            text = textFromArrayText(arrayText: arrayUnicWord, separator: separator)

        }
        TextLabelSecond.text = text
        ActivityIndicator.stopAnimating()
        alert()
    
    }
    
    @IBAction func InsertButtonAction(_ sender: UIButton) {
        if let myString = UIPasteboard.general.string {
            TextLabelFirst.text = myString
            TextLabelSecond.text = """
                Found characters: \(TextLabelFirst.text!.count)
                Please select a text separator and click "Create"
                The application will create a set of non-repeating words
                If you want to keep the original word order - click "Sorted words"
            """
        }
    }
    
    @IBAction func CopyButtonAction(_ sender: UIButton) {
        UIPasteboard.general.string = TextLabelSecond.text
    }

    @IBAction func SelectSeparationAction(_ sender: UIButton) {
        let elementPicker = UIPickerView()
        elementPicker.delegate = self
        elementPicker.center = view.center
        elementPicker.backgroundColor = #colorLiteral(red: 0.8004586101, green: 0.9649798274, blue: 0.009857033379, alpha: 1)
        elementPicker.backgroundColor?.withAlphaComponent(0.2)
        elementPicker.layer.cornerRadius = 10
      
        view.addSubview(elementPicker)
    }
    @IBAction func SortedButton(_ sender: UIButton) {
        switch sorted {
        case true:
            sorted = false
            SortedButtonOutlet.setImage(UIImage(named: "Sorted words off"), for: .normal)
            SortedButtonOutlet.setImage(UIImage(named: "Sorted words off"), for: .highlighted)
            SortedButtonOutlet.setImage(UIImage(named: "Sorted words off"), for: .selected)
            
        case false:
            sorted = true
            SortedButtonOutlet.setImage(UIImage(named: "Sorted words"), for: .normal)
            SortedButtonOutlet.setImage(UIImage(named: "Sorted words"), for: .highlighted)
            SortedButtonOutlet.setImage(UIImage(named: "Sorted words"), for: .selected)

        }
    }
    @IBAction func ViewAsTabButton(_ sender: UIButton) {
        performSegue(withIdentifier: "JumpToSecond", sender: nil)
        
    }
    @IBAction func ThanksButtonAction(_ sender: UIButton) {
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "JumpToSecond" {
            let pageAdd = segue.destination as! TableVC
            pageAdd.FirstScreenInfo = self
        }
    }
    
}



extension FirstScreenVC: UIPickerViewDataSource , UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return separators.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(separators[row])
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == 0 { separator = " " } else {
        separator = separators[row]
        }
        print(separator)
        pickerView.isHidden = true
    }
    
}

extension FirstScreenVC {
    func alert () {
        let allert = UIAlertController(title: "Сongratulation!", message: "Total words: \(arrayWords.count). Created set including \(arrayUnicWord.count) words", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Ok", style: .cancel)
        allert.addAction(cancel)
        allert.editButtonItem.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        present(allert, animated: true)
        
    }
}
